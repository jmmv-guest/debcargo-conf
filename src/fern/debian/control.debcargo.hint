Source: rust-fern
Section: rust
Priority: optional
Build-Depends: debhelper (>= 11),
 dh-cargo (>= 10),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-log-0.4+default-dev <!nocheck>,
 librust-log-0.4+std-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Andrej Shadura <andrewsh@debian.org>
Standards-Version: 4.2.0
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/fern]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/fern

Package: librust-fern-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-log-0.4+default-dev,
 librust-log-0.4+std-dev
Suggests:
 librust-fern+colored-dev (= ${binary:Version}),
 librust-fern+syslog-dev (= ${binary:Version}),
 librust-fern+syslog-3-dev (= ${binary:Version}),
 librust-fern+syslog-4-dev (= ${binary:Version}),
 librust-fern+syslog3-dev (= ${binary:Version})
Provides:
 librust-fern+default-dev (= ${binary:Version}),
 librust-fern+meta-logging-in-format-dev (= ${binary:Version}),
 librust-fern-0-dev (= ${binary:Version}),
 librust-fern-0+default-dev (= ${binary:Version}),
 librust-fern-0+meta-logging-in-format-dev (= ${binary:Version}),
 librust-fern-0.5-dev (= ${binary:Version}),
 librust-fern-0.5+default-dev (= ${binary:Version}),
 librust-fern-0.5+meta-logging-in-format-dev (= ${binary:Version}),
 librust-fern-0.5.7-dev (= ${binary:Version}),
 librust-fern-0.5.7+default-dev (= ${binary:Version}),
 librust-fern-0.5.7+meta-logging-in-format-dev (= ${binary:Version})
Description: Simple, efficient logging - Rust source code
 This package contains the source for the Rust fern crate, packaged by debcargo
 for use with cargo and dh-cargo.

Package: librust-fern+colored-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-fern-dev (= ${binary:Version}),
 librust-colored-1+default-dev (>= 1.5-~~)
Provides:
 librust-fern-0+colored-dev (= ${binary:Version}),
 librust-fern-0.5+colored-dev (= ${binary:Version}),
 librust-fern-0.5.7+colored-dev (= ${binary:Version})
Description: Simple, efficient logging - feature "colored"
 This metapackage enables feature colored for the Rust fern crate, by pulling in
 any additional dependencies needed by that feature.

Package: librust-fern+syslog-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-fern-dev (= ${binary:Version}),
 librust-syslog-4+default-dev
Provides:
 librust-fern-0+syslog-dev (= ${binary:Version}),
 librust-fern-0.5+syslog-dev (= ${binary:Version}),
 librust-fern-0.5.7+syslog-dev (= ${binary:Version})
Description: Simple, efficient logging - feature "syslog"
 This metapackage enables feature syslog for the Rust fern crate, by pulling in
 any additional dependencies needed by that feature.

Package: librust-fern+syslog-3-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-fern-dev (= ${binary:Version}),
 librust-syslog3-3+default-dev
Provides:
 librust-fern-0+syslog-3-dev (= ${binary:Version}),
 librust-fern-0.5+syslog-3-dev (= ${binary:Version}),
 librust-fern-0.5.7+syslog-3-dev (= ${binary:Version})
Description: Simple, efficient logging - feature "syslog-3"
 This metapackage enables feature syslog-3 for the Rust fern crate, by pulling
 in any additional dependencies needed by that feature.

Package: librust-fern+syslog-4-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-fern-dev (= ${binary:Version}),
 librust-syslog-4+default-dev
Provides:
 librust-fern-0+syslog-4-dev (= ${binary:Version}),
 librust-fern-0.5+syslog-4-dev (= ${binary:Version}),
 librust-fern-0.5.7+syslog-4-dev (= ${binary:Version})
Description: Simple, efficient logging - feature "syslog-4"
 This metapackage enables feature syslog-4 for the Rust fern crate, by pulling
 in any additional dependencies needed by that feature.

Package: librust-fern+syslog3-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-fern-dev (= ${binary:Version}),
 librust-syslog3-3+default-dev
Provides:
 librust-fern-0+syslog3-dev (= ${binary:Version}),
 librust-fern-0.5+syslog3-dev (= ${binary:Version}),
 librust-fern-0.5.7+syslog3-dev (= ${binary:Version})
Description: Simple, efficient logging - feature "syslog3"
 This metapackage enables feature syslog3 for the Rust fern crate, by pulling in
 any additional dependencies needed by that feature.
